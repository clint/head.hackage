# vi: set filetype=sh

# Packages expected not to build due to GHC bugs. This is `source`'d by the CI
# script and the arguments in BROKEN_ARGS are added to the hackage-ci
# command-line.

# Mark the named package as broken.
#
# Usage:
#    broken $pkg_name $ghc_ticket_number
#
function broken() {
  pkg_name="$1"
  ticket="$2"
  echo "Marking $pkg_name as broken due to #$ticket"
  EXTRA_OPTS="$EXTRA_OPTS --expect-broken=$pkg_name"
}

# Return the version number of the most recent release of the given package
function latest_version() {
  pkg=$1
  curl -s -H "Accept: application/json" -L -X GET http://hackage.haskell.org/package/$pkg/preferred | jq '.["normal-version"] | .[0]' -r
}

# Add a package to the set of packages that lack patches but are nevertheless
# tested.
function extra_package() {
  pkg_name="$1"
  version="$2"
  if [ -z "$version" ]; then
    version=$(latest_version $pkg_name)
  fi
  echo "Adding $pkg_name-$version to extra package set"
  EXTRA_OPTS="$EXTRA_OPTS --extra-package=$pkg_name==$version"
}

if [ -z "$GHC" ]; then GHC=ghc; fi

function ghc_version() {
  $GHC --version | sed 's/.*version \([0-9]*\.\([0-9]*\.\)*\)/\1/'
}

# ======================================================================
# The lists begin here
#
# For instance:
#
#    broken "lens" 17988

version="$(ghc_version)"
echo "Found GHC $version."
case $version in
  8.8.*)
    #       package              ticket
    broken "parameterized-utils" 17056
    ;;

  8.9.*)
    #       package             ticket
    ;;

  *)
    echo "No broken packages for GHC $version"
    ;;
esac

# Extra packages
extra_package lens
extra_package aeson
extra_package criterion
extra_package scotty
extra_package generic-lens
extra_package microstache
extra_package singletons
extra_package servant
