{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE ScopedTypeVariables #-}

module TestPatches
  ( testPatches
  , Config(..), config
  ) where

import Control.Monad
import Data.Foldable
import Data.List (intercalate)
import Data.Maybe
import Data.Text (Text)
import GHC.Generics
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import qualified Data.Text.Encoding as TE
import qualified Data.Text.Encoding.Error as TE
import qualified Data.ByteString.Char8 as BS
import qualified Data.ByteString.Lazy.Char8 as BSL
import Data.Aeson
import qualified Data.Map.Strict as M
import qualified Data.Map.Merge.Strict as M
import qualified Data.Set as S

import qualified Distribution.Package as Cabal
import Distribution.Text
import Distribution.Types.Version hiding (showVersion)

import qualified Text.PrettyPrint.ANSI.Leijen as PP
import Text.PrettyPrint.ANSI.Leijen (Doc, vcat, (<+>))
import System.FilePath
import System.Directory
import System.Environment (getEnvironment)
import System.Exit
import System.Process.Typed
import System.IO.Temp
import System.IO
import Cabal.Plan
import NeatInterpolation
import Options.Applicative

import Types
import qualified MakeConstraints
import Utils

newtype BrokenPackages = BrokenPackages { getBrokenPackageNames :: S.Set PkgName }
  deriving (Semigroup, Monoid)

failureExpected :: BrokenPackages -> PkgName -> Bool
failureExpected (BrokenPackages pkgs) name = name `S.member` pkgs

data Config = Config { configPatchDir :: FilePath
                     , configCompiler :: FilePath
                     , configGhcOptions :: [String]
                     , configCabalOptions :: [String]
                     , configOnlyPackages :: Maybe (S.Set Cabal.PackageName)
                     , configConcurrency :: Int
                     , configExtraCabalFragments :: [FilePath]
                     , configExtraPackages :: [(Cabal.PackageName, Version)]
                     , configExpectedBrokenPkgs :: BrokenPackages
                     }

cabalOptions :: Config -> [String]
cabalOptions cfg =
  configCabalOptions cfg ++
  [ "-w", configCompiler cfg
  ] ++ concatMap (\opt -> ["--ghc-options", opt]) (configGhcOptions cfg)

config :: Parser TestPatches.Config
config =
  TestPatches.Config
    <$> patchDir
    <*> compiler
    <*> ghcOptions
    <*> cabalOptions
    <*> onlyPackages
    <*> concurrency
    <*> extraCabalFragments
    <*> extraPackages
    <*> expectedBrokenPkgs
  where
    patchDir = option str (short 'p' <> long "patches" <> help "patch directory" <> value "./patches")
    compiler = option str (short 'w' <> long "with-compiler" <> help "path of compiler")
    ghcOptions = many $ option str (short 'f' <> long "ghc-option" <> help "flag to pass to compiler")
    cabalOptions = many $ option str (short 'F' <> long "cabal-option" <> help "flag to pass to cabal-install")
    onlyPackages =
      fmap (Just . S.fromList) (some $ option pkgName (short 'o' <> long "only" <> help "filter packages"))
      <|> pure Nothing
    concurrency = option auto (short 'j' <> long "concurrency" <> value 1 <> help "number of concurrent builds")
    extraCabalFragments = many $ option str (long "extra-cabal-fragment" <> help "path of extra configuration to include in cabal project files")
    extraPackages = many $ option pkgVer (short 'P' <> long "extra-package" <> help "other, un-patched packages to test")
    expectedBrokenPkgs =
      fmap (BrokenPackages . S.fromList) $ many
      $ option
          (fmap toPkgName pkgName)
          (short 'b' <> long "expect-broken" <> metavar "PKGNAME" <> help "expect the given package to fail to build")

    pkgVer :: ReadM (Cabal.PackageName, Version)
    pkgVer = str >>= parse . T.pack
      where
        parse s
          | [name, ver] <- T.splitOn "==" s
          , Just ver' <- simpleParse $ T.unpack ver
          = pure (Cabal.mkPackageName $ T.unpack name, ver')
          | otherwise
          = fail $ unlines
              [ "Invalid extra package specified:"
              , "expected to be in form of PKG_NAME==VERSION"
              ]

    pkgName :: ReadM Cabal.PackageName
    pkgName = str >>= maybe (fail "invalid package name") pure . simpleParse

testPatches :: Config -> IO ()
testPatches cfg = do
  setup cfg
  packages <- findPatchedPackages (configPatchDir cfg)
  packages <- return (packages ++ configExtraPackages cfg)
  let packages' :: S.Set (Cabal.PackageName, Version)
      packages'
        | Just only <- configOnlyPackages cfg
        = S.fromList $ filter (\(pname,_) -> pname `S.member` only) packages
        | otherwise
        = S.fromList packages

  let build :: (Cabal.PackageName, Version) -> IO [TestedPatch LogOutput]
      build (pname, ver) = do
        res <- buildPackage cfg pname ver
        let tpatch = TestedPatch { patchedPackageName = PkgName $ T.pack $ display pname
                                 , patchedPackageVersion = Ver $ versionNumbers ver
                                 , patchedPackageResult = res
                                 }
        return [tpatch]
  testedPatches <- fold <$> mapConcurrentlyN (fromIntegral $ configConcurrency cfg) build (S.toList packages')
  let runResult = RunResult testedPatches

  print $ resultSummary (configExpectedBrokenPkgs cfg) runResult
  BSL.writeFile "results.json" . encode =<< writeLogs "logs" runResult
  let failedBuilds = failedUnits (configExpectedBrokenPkgs cfg) runResult
      planningFailures = planningErrors runResult
      okay = null failedBuilds && null planningFailures
  unless okay $ exitWith $ ExitFailure 1

writeLogs :: FilePath -> RunResult LogOutput -> IO (RunResult ())
writeLogs logDir runResult = do
    createDirectoryIfMissing True logDir
    let failedUnits = [ (unitId, log)
                      | (unitId, (buildInfo, result)) <- M.toList $ runResultUnits runResult
                      , Just log <- pure $
                          case result of
                            BuildSucceeded log -> Just log
                            BuildFailed log -> Just log
                            _ -> Nothing
                      ]
    mapM_ writeLog failedUnits
    return (() <$ runResult)
  where
    writeLog (UnitId unitId, LogOutput log) = TIO.writeFile logPath log
      where logPath = logDir </> T.unpack unitId

failedUnits :: BrokenPackages -> RunResult log
            -> M.Map UnitId (BuildInfo, BuildResult log)
failedUnits broken = M.filter didFail . runResultUnits
  where
    didFail (buildInfo, result) =
      case result of
        BuildFailed _ -> not $ failureExpected broken (pkgName buildInfo)
        _             -> False

planningErrors :: RunResult log -> [(PkgName, Ver)]
planningErrors runResult =
  [ (patchedPackageName tpatch, patchedPackageVersion tpatch)
  | tpatch <- testedPatches runResult
  , PackagePlanningFailed _ <- pure $ patchedPackageResult tpatch
  ]

resultSummary :: forall log. BrokenPackages -> RunResult log -> Doc
resultSummary broken runResult = vcat
  [ "Total units built:" <+> pshow (length allUnits)
  , ""
  , pshow (length planningErrs) <+> "had no valid install plan:"
  , PP.indent 4 $ vcat $ map (uncurry prettyPkgVer) planningErrs
  , ""
  , pshow (length failedUnits) <+> "units failed to build:"
  , PP.indent 4 $ vcat
    [ prettyPkgVer (pkgName binfo) (version binfo) <+> expectedDoc
    | (binfo, _) <- M.elems failedUnits
    , let expectedDoc
            | failureExpected broken (pkgName binfo) = PP.parens $ PP.yellow $ PP.text "expected"
            | otherwise = mempty
    ]
  , ""
  , pshow (length failedDependsUnits) <+> "units failed to build due to unbuildable dependencies."
  ]
  where
    allUnits = runResultUnits runResult
    planningErrs = planningErrors runResult

    failedUnits :: M.Map UnitId (BuildInfo, BuildResult log)
    failedUnits = M.filter failed allUnits
      where failed (_, BuildFailed _) = True
            failed _ = False

    failedDependsUnits :: M.Map UnitId (S.Set UnitId)
    failedDependsUnits = M.filter (not . S.null) (failedDeps allUnits)

toPkgName :: Cabal.PackageName -> PkgName
toPkgName = PkgName . T.pack . display

toVer :: Version -> Ver
toVer = Ver . versionNumbers

-- | For @cabal-plan@ types.
prettyPkgVer :: PkgName -> Ver -> Doc
prettyPkgVer (PkgName pname) (Ver ver) =
  PP.blue (PP.text $ T.unpack pname) <+> PP.green (PP.text $ intercalate "." $ map show ver)

-- | For @Cabal@ types.
prettyPackageVersion :: Cabal.PackageName -> Version -> Doc
prettyPackageVersion pname version =
  prettyPkgVer (toPkgName pname) (toVer version)

buildPackage :: Config -> Cabal.PackageName -> Version -> IO (PackageResult LogOutput)
buildPackage cfg pname version = do
  logMsg $ "=> Building" <+> prettyPackageVersion pname version
  compilerId <- getCompilerId (configCompiler cfg)

  -- prepare the test package
  createDirectoryIfMissing True dirName
  copyFile "cabal.project" (dirName </> "cabal.project")
  appendFile (dirName </> "cabal.project") "packages: .\n"
  TIO.writeFile
    (dirName </> concat ["test-", display pname, ".cabal"])
    (makeTestCabalFile pname version)

  -- run the build
  code <- runProcess $ setWorkingDir dirName
                     $ proc "cabal"
                     $ ["new-build"] ++ cabalOptions cfg

  -- figure out what happened
  let planPath = dirName </> "dist-newstyle" </> "cache" </> "plan.json"
  planExists <- doesFileExist planPath
  case planExists of
    True -> do
      Just plan <- decode <$> BSL.readFile planPath :: IO (Maybe PlanJson)
      cabalDir <- getCabalDirectory
      let logDir = cabalDir </> "logs" </> compilerId
      results <- mapM (checkUnit logDir) (pjUnits plan)
      logMsg $
        let result = case code of
              ExitSuccess -> PP.cyan "succeeded"
              ExitFailure n -> PP.red "failed" <+> PP.parens ("code" <+> pshow n)
        in "=> Build of" <+> prettyPackageVersion pname version <+> result
      return $ PackageResult (code == ExitSuccess) (mergeInfoPlan (planToBuildInfo plan) results)
    False -> do
      logMsg $ PP.red $ "=> Planning for" <+> prettyPackageVersion pname version <+> "failed"
      return $ PackagePlanningFailed mempty
  where
    planToBuildInfo :: PlanJson -> M.Map UnitId BuildInfo
    planToBuildInfo plan = M.fromList
      [ (uId unit, info)
      | unit <- M.elems $ pjUnits plan
      , let depends :: S.Set UnitId
            depends = fold
              [ ciLibDeps comp <> ciExeDeps comp
              | comp <- M.elems $ uComps unit
              ]
      , let PkgId pname pvers = uPId unit
      , let info = BuildInfo { pkgName = pname
                             , version = pvers
                             , flags = uFlags unit
                             , dependencies = depends
                             }
      ]

    checkUnit :: FilePath -> Unit -> IO (BuildResult LogOutput)
    checkUnit logDir unit
      | UnitTypeBuiltin <- uType unit = return BuildPreexisted
      | UnitTypeLocal <- uType unit = return $ BuildSucceeded (LogOutput "<<inplace>>")
      | otherwise = do
      exists <- doesFileExist logPath
      case exists of
        True -> do
          buildLog <- TE.decodeUtf8With TE.lenientDecode <$> BS.readFile logPath
          if | T.null buildLog
               -> return $ BuildFailed (LogOutput buildLog)
             | any isInstallingLine $ take 5 $ reverse $ T.lines buildLog
               -> return $ BuildSucceeded (LogOutput buildLog)
             | otherwise
               -> return $ BuildFailed (LogOutput buildLog)
        False -> return BuildNotAttempted
      where
        isInstallingLine line = "Installing" `T.isPrefixOf` line
        logPath =
          case uId unit of
            UnitId uid -> logDir </> T.unpack uid <.> "log"

    mergeInfoPlan :: Ord k
                  => M.Map k BuildInfo
                  -> M.Map k (BuildResult log)
                  -> M.Map k (BuildInfo, BuildResult log)
    mergeInfoPlan = M.merge err err (M.zipWithMatched $ \_ x y -> (x,y))
      where
        err = M.mapMissing $ \_ _ -> error "error merging"

    dirName = "test-" ++ display pname ++ "-" ++ display version

makeTestCabalFile :: Cabal.PackageName -> Version -> T.Text
makeTestCabalFile pname' ver' =
  [text|
    cabal-version:       2.2
    name: test-$pname
    version: 1.0

    library
      exposed-modules:
      build-depends: $pname == $ver
      default-language: Haskell2010
  |]
  where
    pname = T.pack $ display pname'
    ver = T.pack $ display ver'

setup :: Config -> IO ()
setup cfg = do
  keysExist <- doesDirectoryExist "keys"
  unless keysExist $ do
    cabalDir <- getCabalDirectory
    -- Work around cabal-install bug; it seems to get confused by repository changes
    removePathForcibly $ cabalDir </> "packages" </> repoName
    createDirectoryIfMissing True $ cabalDir </> "packages" </> repoName
    runProcess_ $ proc "build-repo.sh" ["gen-keys"]

  cwd <- getCurrentDirectory
  environ <- getEnvironment
  let env = environ ++
            [ ("REPO_NAME", repoName)
            , ("REPO_URL", "file://" ++ (cwd </> "repo"))
            , ("PATCHES", configPatchDir cfg)
            ]

  removePathForcibly "cabal.project"
  runProcess_
    $ setEnv env
    $ proc "build-repo.sh" ["build-repo"]

  projectFile <- openFile "cabal.project" WriteMode
  runProcess_
    $ setStdout (useHandleClose projectFile)
    $ setEnv env
    $ proc "build-repo.sh" ["build-repository-blurb"]

  extraFragments <- mapM readFile (configExtraCabalFragments cfg)
  constraints <- MakeConstraints.makeConstraints (configPatchDir cfg)
  appendFile "cabal.project" $ show $ vcat $
    [ "with-compiler: " <> PP.text (configCompiler cfg)
    , MakeConstraints.allowNewer MakeConstraints.bootPkgs
    ] ++ map PP.text extraFragments

  runProcess_ $ proc "cabal" ["new-update"]

  -- Force cabal to rebuild the index cache.
  buildPackage cfg "acme-box" (mkVersion [0,0,0,0])
  return ()
  where
    repoName = "local"

-- | Compute for each unit which of its dependencies failed to build.
failedDeps :: M.Map UnitId (BuildInfo, BuildResult log) -> M.Map UnitId (S.Set UnitId)
failedDeps pkgs =
  let res = fmap f pkgs -- N.B. Knot-tied

      f :: (BuildInfo, BuildResult log) -> S.Set UnitId
      f (binfo, result) =
        failedDirectDeps <> failedTransDeps
        where
          failedTransDeps = S.unions $ map (res M.!) (S.toList $ dependencies binfo)
          failedDirectDeps = S.filter failed $ S.filter excludeSelf (dependencies binfo)

          -- We don't want failures of units in the same package to count as
          -- failed dependencies.
          excludeSelf :: UnitId -> Bool
          excludeSelf unitId = pkgName binfo /= pkgName binfo'
            where (binfo', _) = pkgs M.! unitId

          failed :: UnitId -> Bool
          failed unitId =
            case snd $ pkgs M.! unitId of
              BuildFailed _ -> True
              _ -> False
  in res
